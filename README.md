### Documentation of Rhythms-Entropy

#### Figures reported in Kosciessa et al. (2020, PLOS CB) are produced in the following scripts:
* 1A:     simulations/H_CoarseGrainingExample
* 1B:     simulations/D_RparamExample
* 3A:     simulations/D_RparamExample
* 3B:     empirical/Z_IntroFigure_Fig3
* 4:      simulations/C1_multiFreqMSE_plot
* 5:      simulations/C1_multiFreqMSE_plot
* 6A:     empirical/mse/F_ComparisonWithPSD
* 6B-D:   empirical/mse/G_FigureTopographySummary
* 7:      empirical/mse/H_MSE_ageDifferences_overview
* 8:      empirical/mse/I_FixedRInfluenceOnCoarseMSE
* 9:      empirical/mse/J_FineScalePSDassociation
* 10:     empirical/mse/K_overviewPlot_bpMSE_Rate
* 11A: 	  simulations/K_transientSlopeMod/K2_calculateSampEn
* 11B: 	  empirical/ebosc/F_plotEventRate_total
* 11B: 	  empirical/perirhythm_entropy/G_plotExemplaryAlpha
* 11C:    empirical/perirhythm_entropy/E1_msePlot_low
* 11D:    empirical/perirhythm_entropy/E2_msePlot_high
* 11CD:   empirical/perirhythm_entropy/F_FFT
* 12A:    empirical/mse/Z_SummaryFigure


----

* S2:     simulations/I_SimulationWNOneF
* S3:     simulations/F_saveExemplaryAlphaTraces
* S4:     simulations/G_PlotFilterResponse
* S5:     empirical/fft/C2_CBPA_age_condition_plot
* S6:     empirical/J_FineScalePSDassociation
* S7:     simulations/J3_complexityMSE_plot
* S8:     empirical/perirhythm_entropy/B2_InspectRhythmAlignment
* S9A:    empirical/mse_surrogate/H_plotShuffledTraces
* S9BC:    empirical/mse_surrogate/E2_MSE_ageDifferences_overview
* S9DE:    empirical/mse_surrogate/F2_ageDifferences_originalVSshuffled